module gitee.com/youbeiwuhuan/knife4go/gin-swagger-knife-example

go 1.15

require (
	gitee.com/youbeiwuhuan/knife4go/gin-swagger-knife v0.0.0-20230225061513-93228721cd10
	github.com/gin-gonic/gin v1.7.4
)
